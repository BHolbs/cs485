import string
letters = list(string.ascii_lowercase)

cipher = input("Cipher: ")
cipher = cipher.strip()
cipher = cipher.lower()
print("")

if (cipher.isalpha()):
    for i in range(0, 27):
        print("Shift of " + str(i))
        out = []

        for letter in cipher:
            val = (letters.index(letter) + i) % 26
            new = letters[val]
            out.append(new)

        print(''.join(out))
        print("")
else:
    print("Cipher is not purely alphabetical characters.")
