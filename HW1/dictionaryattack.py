from tqdm import tqdm
from itertools import starmap, cycle
import string

cipher = input("Cipher: ")
cipher = cipher.strip()
cipher = cipher.upper()

# most common trigrams in english
trigrams = ['THE', 'AND', 'THA', 'ENT', 'ING', 'ION', 'TIO', 'FOR',
            'NDE', 'HAS', 'NCE', 'EDT', 'TIS', 'OFT', 'STH', 'MEN']
letters = list(string.ascii_uppercase)

# Borrowed from Rosetta Code's entry for the Vigenere cipher
def decode(ciphertext, key):
    def dec(c, k):
        return chr(((ord(c) - ord(k) - 2 * ord('A')) % 26 + ord('A')))

    return ''.join(starmap(dec, zip(ciphertext, cycle(key))))


# quick little helper function to get the dictionary into memory
def setupDict():
    with open('words_alpha.txt') as file:
        return [line.rstrip('\n').upper() for line in file]


#perform a dictionary attack on the vigenere cipher
def dictAttack():
    print("Performing dictionary attack...")
    words = setupDict()
    for i in tqdm(range(0, len(words))):
        # skip words not in bounds, thanks for the extra info!
        if len(words[i]) < 5 or len(words[i]) > 10:
            continue

        attempt = decode(cipher, words[i])
        count = 0
        for j in range(len(trigrams)):
            if trigrams[j] in attempt:
                count += 1

        if count >= 13:
            print("")
            print("Attempt with " + str(count) + " trigrams with key " + words[i])
            print(attempt)
            print("")
            cont = input("Does this appear to be an unencrypted message? (y\\n): ")
            if cont.lower() == 'y':
                exit()

    # if we get all the way here, the dictionary attack has failed
    print("Did not find suitable keyword with dictionary attack.")


dictAttack()
