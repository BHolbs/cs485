# CS485

Code repo for PSU CS-485 (Cryptography)

While this repository is now public, I kindly ask that any students at PSU that happen upon this repository to not copy any of its contents. Academic  Academic Misconduct is taken very seriously at PSU, and can get you (and me) into serious trouble.
Please visit https://www.pdx.edu/dos/academic-misconduct for more information.

This code is licenced under the MIT License, with the exception of `words_alpha.txt` which is licensed under The Unlicense, from a repository located [here](https://github.com/dwyl/english-words).
See LICENSE file in the root of the git repo for more information on files not excepted.

