import random


def is_prime(n, k):
    if n != int(n):
        return False

    n = int(n)

    # some base cases
    if n == 0 or n == 1 or n == 4 or n == 6 or n == 8 or n == 9:
        return False

    if n == 2 or n == 3 or n == 5 or n == 7:
        return True

    s = 0
    d = n - 1
    while d % 2 == 0:
        d >>= 1
        s += 1

    assert (2 ** s * d == n - 1)

    def miller_rabin(x):
        if pow(x, d, n) == 1:
            return False
        for j in range(s):
            if pow(x, 2 ** j * d, n) == n - 1:
                return False

        return True

    for i in range(k):
        a = random.randrange(2, n)
        if miller_rabin(a):
            return False

    return True
