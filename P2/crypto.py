import keygen
from tqdm import tqdm

def encrypt(key, m):
    # convert message to binary
    mbytes = ' '.join(format(ord(x), 'b') for x in m)
    mbytes = mbytes.split(' ')
    for i, byte in enumerate(mbytes):
        while len(mbytes[i]) != 8:
            mbytes[i] = '0' + mbytes[i]
    bits = ''.join(mbytes)

    # break message up into 32 bit chunks, really 31 bit chunks because the 32nd bit is never high since we're in ascii
    # this is not very pythonic, but i'd rather be able to debug it sensibly
    fullchunks = int(len(bits)/32)
    leftover = len(bits) % 32
    chunks = list()
    for i in range(0, fullchunks):
        chunks.append(bits[(i*32): (i*32)+32])
    chunks.append(bits[fullchunks*32:len(bits)])

    ciphertext = ''

    print('Encrypting...')
    for chunk in tqdm(chunks):
        k = keygen.random.randint(0, key.p-1)
        c1 = pow(key.g, k, key.p)
        c2 = (pow(key.e, k, key.p)*(int(chunk, 2) % key.p)) % key.p

        ciphertext += str(c1) + ',' + str(c2) + "\n"

    file = open("ctext.txt", "w")
    file.write(ciphertext)
    file.close()


def decrypt(key, c):
    lines = c.split('\n')
    lines = lines[:-1]
    chunks = list()
    for line in lines:
        pieces = line.split(',')
        chunks.append(pieces)

    plaintext = ''

    # go through each pair of ciphertext
    print('Decrypting...')
    for chunk in tqdm(chunks):
        m = (pow(int(chunk[0]), key.p - 1 - key.d, key.p) * (int(chunk[1]) % key.p)) % key.p

        # convert ciphertext to binary strings of length % 8 = 0
        # so we know we have a character for each byte
        mBin = bin(m)[2:]
        while len(mBin) % 8 != 0:
            mBin = '0' + mBin

        # go through each byte, translating to ASCII
        curr = ''
        for i in range(len(mBin)):
            curr = curr + mBin[i]

            if len(curr) == 8 and i != 0:
                ch = chr(int(curr, 2))
                curr = ''
                plaintext = plaintext + ch

    print('Decrypted: ' + plaintext)


