# Public Key Cryptosystem
Written by: Blaine Holbert

# Requirements
1. Python 3.75
2. `tqdm` for progress bars: this can be installed automatically along with any other requirements with the command `pip3 install -r requirements.txt`
3. `ptext.txt` which is the message to encrypt
4. `ctext.txt` which is the message to decrypt, this will be generated if `ptext` exists and has been encrypted

# Running the Program
This program is a python script, with the entry point in `pubcrypt.py`, so to run the cryptosystem, simply execute this from the command line while in the directory: `python3 pubcrypt.py`.
