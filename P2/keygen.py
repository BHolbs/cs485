from millerrabin import is_prime
import random

# constants, don't mess with them otherwise
LOW = 1073741824
HIGH = 2147483647


class PubKey:
    def __init__(self, p, g, e):
        self.p = int(p)
        self.g = int(g)
        self.e = int(e)


class PriKey:
    def __init__(self, p, g, d):
        self.p = int(p)
        self.g = int(g)
        self.d = int(d)


def genKey(seed):
    seed = int(seed)
    random.seed(seed)
    q = random.randint(LOW, HIGH)
    p = (2 * q) + 1

    # set to stop us from trying the same q for p over and over
    qcandidates = set()

    while not is_prime(p, 32):
        while not is_prime(q, 32) or q % 12 != 5:
            q = random.randint(LOW, HIGH)

        if q in qcandidates:
            q = random.randint(LOW, HIGH)
            continue
        qcandidates.add(q)
        p = (2 * q) + 1

    print('q = ' + str(q))
    print('p = ' + str(p))

    g = 2
    d = 0
    while not 0 < d < p:
        d = input('Select a d such that 0 < d < p: ')
        d = int(d)

    e = pow(g, d, p)

    file = open("pubkey.txt", "w")
    file.write(str(p) + ' ' + str(g) + ' ' + str(e))
    file.close()

    file = open("prikey.txt", "w")
    file.write(str(p) + ' ' + str(g) + ' ' + str(d))
    file.close()
