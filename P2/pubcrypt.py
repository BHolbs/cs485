import keygen
import crypto

print("Options:")
print("\tGenerate keys (g):")
print("\tEncrypt ptext.txt (e):")
print("\tDecrypt citext.txt (d):")

choice = input("Choice: ")
while choice.lower() != 'g' and choice.lower() != 'e' and choice.lower() != 'd':
    choice = input("Choice: ")

if choice.lower() == 'g':
    seed = input("Seed for random number generation: ")
    seed = int(seed)
    keygen.genKey(seed)
elif choice.lower() == 'e':
    file = open("pubkey.txt", "r")
    k = file.read()
    tok = k.split(' ')
    key = keygen.PubKey(tok[0], tok[1], tok[2])
    file.close()

    file = open("ptext.txt", "r")
    message = file.read()
    file.close()

    crypto.encrypt(key, message)
elif choice.lower() == 'd':
    file = open("prikey.txt", "r")
    k = file.read()
    tok = k.split(' ')
    key = keygen.PriKey(tok[0], tok[1], tok[2])
    file.close()

    file = open("ctext.txt" , "r")
    message = file.read()
    file.close()

    crypto.decrypt(key, message)
