## PSU-CRYPT

PSU-Crypt is a symmetric block cipher, that is a hybrid of Twofish (originally designed by Bruce Schneier, John Kelsey, Doug Whiting, David Wagner, Chris Hall), and SKIPJACK, developed by the National Security Administration (NSA) of the United States. The block size is 64 bits, and makes use of a 64 bit key.

# Required Files
There are currently no implemented safeguards to notify user of missing files, the program will simply crash. Please have the files listed below for each option. Default files are not included with this repository.

## To Encrypt AND Subsequently Decrypt
`plaintext.txt` - the message to be encrypted, in plain, ASCII text.

`key.txt` - the 64 bit key to be used for encryption, the program expects this file to be a 64 bit hexadecimal number **WITHOUT** the leading `0x`.

## To Decrypt a Previously Encrypted Message by PSU-CRYPT.
`ciphertext.txt` - the encrypted message, produced by encryption automatically, in hex.

`key.txt` - the 64 bit key to be used for encryption, the program expects this file to be a 64 bit hexadecimal number **WITHOUT** the leading `0x`.
