#include "psu-crypt.hpp"

int main(int argc, char* argv[]) {
    std::cout << "Initializing..." << std::endl;
    psu_crypt c("plaintext.txt", "key.txt");
    std::cout << "Initialized." << std::endl;

    std::cout << "Options: " << std::endl;
    std::cout << "\tEncrypt plaintext.txt only (e)" << std::endl;
    std::cout << "\tDecrypt ciphertext.txt only (d)" << std::endl;
    std::cout << "\tEncrypt plaintext.txt and decrypt resulting ciphertext.txt (a)" <<std::endl;

    char choice = '\0';
    while (tolower(choice) != 'e' && tolower(choice) != 'd' && tolower(choice) != 'a') {
        std::cout << "Selection: ";
        std::cin >> choice;
    }

    switch (choice) {
        case 'e':
            c.encrypt();
            break;
        case 'd':
            c.decrypt();
            break;
        case 'a':
            c.encrypt();
            c.decrypt();
            break;
    }
}
