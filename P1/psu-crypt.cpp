#include "psu-crypt.hpp"

/* Helper function to perform XOR on strings that are in decimal on the LHS, and hex on the RHS, only used in
   G function, so output is just a 2 character string
   */
std::string xorStringsDecLHexR(std::string l, std::string r) {
    uint16_t lhs = stoull(l, nullptr, 10);
    uint16_t rhs = stoull(r, nullptr, 16);
    uint16_t result = lhs ^ rhs;
    std::stringstream ss;
    ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)result;
    std::string ret = ss.str();
    ss.str(std::string());
    return ret;
}

/*
   Helper function to perform XOR on strings that are both hexadecimal
   */
std::string xorStringsHexRL(std::string l, std::string r) {
    uint16_t lhs = stoull(l, nullptr, 16);
    uint16_t rhs = stoull(r, nullptr, 16);
    uint16_t result = lhs ^ rhs;
    std::stringstream ss;
    ss << std::hex << std::setfill('0') << std::setw(4) << (unsigned int)result;
    std::string ret = ss.str();
    ss.str(std::string());
    return ret;
}

/*
 * Feistel function
 */
std::pair<std::string, std::string> psu_crypt::F(std::string r0, std::string r1, uint8_t round) {
    std::string t0 = G(r0, round),
        t1 = G(r1, round);

    std::cout << "t0: " << t0 + " " << "t1: " << t1 + " " << std::endl;

    //read in values for concatenation of the subkeys, then concatenate
    std::stringstream ss;
    ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)subkeys.at(round).at(subkeyI++);
    std::string l = ss.str();
    ss.str(std::string()); //clear the stream

    ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)subkeys.at(round).at(subkeyI++);
    std::string r = ss.str();
    ss.str(std::string()); //clear the stream
    std::string keyconcat = l + r;

    //calculate f0
    uint32_t tNought = stoull(t0, nullptr, 16), t1times2 = (2 * stoull(t1, nullptr, 16)), keycat = stoull(keyconcat, nullptr, 16);
    uint16_t f0 = (tNought + t1times2 + keycat) % 65536;


    //do it all over again for f1
    ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)subkeys.at(round).at(subkeyI++);
    l = ss.str();
    ss.str(std::string()); //clear the stream

    ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)subkeys.at(round).at(subkeyI++);
    r = ss.str();
    ss.str(std::string());
    keyconcat = l + r;

    uint32_t t0times2 = 2 * stoull(t0, nullptr, 16), tOne = stoull(t1, nullptr, 16);
    keycat = stoull(keyconcat, nullptr, 16);
    uint16_t f1 = (t0times2 + tOne + keycat) % 65536;

    std::cout << std::hex << "f0: " << f0 << " " << "f1: " << f1 << " " << std::endl;

    //since we're doing string representations, make sure leading zeros are there
    std::string fZero, fOne;
    ss << std::hex << std::setw(2) << f0;
    fZero = ss.str();
    ss.str(std::string());

    ss << std::hex << std::setw(2) << f1;
    fOne = ss.str();
    ss.str(std::string());

    return make_pair(fZero, fOne);
}

/*
   G function for Feistel Function
   */
std::string psu_crypt::G(std::string w, uint8_t round) {
    auto subkeysThisCall = subkeys.at(round);
    std::string g1, g2, g3, g4, g5, g6;
    g1 = w.substr(0, 2);
    g2 = w.substr(2, 2);

    g3 = xorStringsDecLHexR(std::to_string(ftable[stoull(g2, nullptr, 16) ^ subkeysThisCall.at(subkeyI++)]), g1);
    g4 = xorStringsDecLHexR(std::to_string(ftable[stoull(g3, nullptr, 16) ^ subkeysThisCall.at(subkeyI++)]), g2);
    g5 = xorStringsDecLHexR(std::to_string(ftable[stoull(g4, nullptr, 16) ^ subkeysThisCall.at(subkeyI++)]), g3);
    g6 = xorStringsDecLHexR(std::to_string(ftable[stoull(g5, nullptr, 16) ^ subkeysThisCall.at(subkeyI++)]), g4);
    std::cout << "g1: " << g1 + " " << "g2: " << g2 + " " << "g3: " << g3 + " " << "g4: " << g4 + " " << "g5: " << g5 + " " << "g6: " << g6 + " " << std::endl;

    return g5 + g6;
}

/*
   Generates the key schedule, called in the constructor
   */
void psu_crypt::generateKeys(uint64_t k) {
    subkeys.resize(ROUNDS);
    std::bitset<64> keybits(k);
    uint8_t rounds = 0;
    uint8_t offset = 0;
    uint8_t byteIndexes[8] = { 7, 6, 5, 4, 3, 2, 1, 0 };
    for (int i = 0; i < SUBKEYS; ++i) {
        rotateL(keybits, 1);
        uint8_t byte = (4 * rounds + offset) % 8;
        std::string sub = keybits.to_string().substr(byteIndexes[byte] * 8, 8);
        uint8_t subkey = stoul(sub, nullptr, 2);
        subkeys.at(rounds).push_back(subkey);

        if ((i + 1) % 12 == 0 && i != 0)
            rounds++;

        offset++;
        if (offset == 4)
            offset = 0;
    }
}

/*
   Helper function for converting an ascii string to a string of the hex values representing that string
   */
std::string psu_crypt::asciiToHex(std::string in) {
    std::string out = "";
    for (uint16_t i = 0; i < in.length(); ++i) {
        unsigned int v = in.at(i);
        std::stringstream ss;
        ss << std::hex << std::setfill('0') << std::setw(2) << (unsigned int)v;
        out.append(ss.str());
        ss.str(std::string());
    }
    return out;
}

/*
   Helper function for whitening a block at the beginning of a round
   */
std::vector<std::string> psu_crypt::whiten(std::string in) {
    std::vector<std::string> words;
    words.push_back(in.substr(0, 4));
    words.push_back(in.substr(4, 4));
    words.push_back(in.substr(8, 4));
    words.push_back(in.substr(12, 4));

    std::vector<std::string> ret;
    for (uint8_t i = 0; i < 4; ++i) {
        uint16_t k = stoul(keyTok.at(i), nullptr, 16);
        uint16_t r = stoul(words.at(i), nullptr, 16);
        uint16_t result = r ^ k;
        std::stringstream ss;
        ss << std::hex << std::setfill('0') << std::setw(4) << (unsigned int)result;
        ret.push_back(ss.str());
        ss.str(std::string());
    }
    return ret;
}

/*
 * Constructor
 */
psu_crypt::psu_crypt(std::string plain, std::string key_file) {
    //get plaintext
    std::ifstream t(plain);

    char tmp[8];
    while (t.read(tmp, 8)) {
        std::string s = tmp;
        s = s.substr(0, 8);
        plain_blocks.push_back(asciiToHex(s));
    }

    uint8_t charsread = t.gcount();
    if (charsread < 8 && charsread > 0) {
        std::string s = tmp;
        s = s.substr(0, charsread-1);
        for (uint8_t i = 0; i != (8 - charsread); ++i) {
            s = s + " ";
        }
        if (s.find_first_not_of(" ") != std::string::npos)
            plain_blocks.push_back(asciiToHex(s.substr(0, 8)));
    }

    //get key
    std::ifstream tk(key_file);

    std::vector<std::string> blocks;
    char tmpk[4];
    std::string as_str = "";
    while (tk.read(tmpk, 4)) {
        std::string s = tmpk;
        keyTok.push_back(s.substr(0, 4));
        as_str.append(s.substr(0, 4));
    }
    key = stoull(as_str, nullptr, 16);
    keyStr = as_str;

    //generate subkeys
    generateKeys(key);
}

/*
   Public facing encryption method, writes ciphertext to a file, ciphertext.txt
   */
void psu_crypt::encrypt() {
    std::string ciphertext = "";
    std::cout << "BEGINNING ENCRYPTION..." << std::endl;
    for (uint64_t i = 0; i < plain_blocks.size(); ++i) {
        std::cout << "Plaintext block " << i << " (as hex): " << plain_blocks.at(i) << std::endl;
        //whiten the block
        std::vector<std::string> whitenedBlock = whiten(plain_blocks.at(i));
        std::string r0, r1, r2, r3;
        for (uint64_t r = 0; r < ROUNDS; ++r) {
            std::cout << "-----------------------------------------------------------" << std::endl;
            std::cout << std::dec << "Beginning round " << r << " of block " << i << "..." << std::endl;
            auto subkeysThisRound = subkeys.at(r);
            std::cout << "Keys: ";
            for (uint8_t key : subkeysThisRound) {
                std::cout << std::hex << (unsigned int)key << " ";
            }
            std::cout << "\n";

            if (r == 0) {
                r0 = whitenedBlock.at(0);
                r1 = whitenedBlock.at(1);
                r2 = whitenedBlock.at(2);
                r3 = whitenedBlock.at(3);
            }


            std::pair<std::string, std::string> fResult = F(r0, r1, r);
            std::string r2tmp = r2, r3tmp = r3;
            r2 = r0;
            r3 = r1;
            r0 = xorStringsHexRL(fResult.first, r2tmp);
            r1 = xorStringsHexRL(fResult.second, r3tmp);
            subkeyI = 0; // reset subkey counter
            std::cout << "Block: " << r0 << r1 << r2 << r3 << std::endl;
            std::cout << std::dec << "End of round " << r << std::endl;
        }

        std::string y0 = r2, y1 = r3, y2 = r0, y3 = r1;
        std::string c0 = xorStringsHexRL(y0, keyTok.at(0)),
            c1 = xorStringsHexRL(y1, keyTok.at(1)),
            c2 = xorStringsHexRL(y2, keyTok.at(2)),
            c3 = xorStringsHexRL(y3, keyTok.at(3));

        ciphertext.append(c0 + c1 + c2 + c3);
    }

    std::ofstream out("ciphertext.txt");
    out << ciphertext;
    out.close();
}

/*
   Public facing decryption method
   */
void psu_crypt::decrypt() {
    std::string deciphered = "";
    std::cout << "BEGINNING DECRYPTION..." << std::endl;

    //get input from ciphertext, borrow this code from constructor
    std::ifstream t("ciphertext.txt");
    std::vector<std::string> cipher_blocks;
    char tmp[32];
    while (t.read(tmp, 16)) {
        std::string s = tmp;
        s = s.substr(0, 16);
        cipher_blocks.push_back(s);
    }
    //don't need to worry about padding, encryption did that for us
    t.close();

    for (unsigned int i = 0; i < cipher_blocks.size(); ++i) {
        std::cout << "Cipher block " << i << ": " << cipher_blocks.at(i) << std::endl;
        //whiten the block
        std::vector<std::string> whitenedBlock = whiten(cipher_blocks.at(i));
        std::string r0, r1, r2, r3;
        for (int r = ROUNDS - 1; r >= 0; --r) {
            std::cout << "-----------------------------------------------------------" << std::endl;
            std::cout << std::dec << "Beginning round " << r << " of block " << i <<  "..." << std::endl;
            auto subkeysThisRound = subkeys.at(r);
            std::cout << "Keys: ";
            for (uint8_t key : subkeysThisRound) {
                std::cout << std::hex << (unsigned int)key << " ";
            }
            std::cout << "\n";

            if (r == 15) {
                r0 = whitenedBlock.at(0);
                r1 = whitenedBlock.at(1);
                r2 = whitenedBlock.at(2);
                r3 = whitenedBlock.at(3);
            }


            std::pair<std::string, std::string> fResult = F(r0, r1, r);
            std::string r2tmp = r2, r3tmp = r3;
            r2 = r0;
            r3 = r1;
            r0 = xorStringsHexRL(fResult.first, r2tmp);
            r1 = xorStringsHexRL(fResult.second, r3tmp);
            subkeyI = 0; // reset subkey counter
            std::cout << "Block: " << r0 << r1 << r2 << r3 << std::endl;
            std::cout << std::dec << "End of round " << r << std::endl;
        }

        std::string y0 = r2, y1 = r3, y2 = r0, y3 = r1;
        std::string c0 = xorStringsHexRL(y0, keyTok.at(0)),
            c1 = xorStringsHexRL(y1, keyTok.at(1)),
            c2 = xorStringsHexRL(y2, keyTok.at(2)),
            c3 = xorStringsHexRL(y3, keyTok.at(3));

        deciphered.append(c0 + c1 + c2 + c3);
    }

    std::cout << "Deciphered plaintext(in hex): " << deciphered << std::endl;

    char choice = '\0';
    while(tolower(choice) != 'y' && tolower(choice) != 'n') {
        std::cout << "Display as text? (y\\n): ";
        std::cin >> choice;
    }

    if(tolower(choice) == 'y') {
        //output deciphered to console
        std::string plain = "";
        for (unsigned int i = 0; i < deciphered.length(); i += 2) {
            char c = stoull(deciphered.substr(i, 2), nullptr, 16);
            plain.append(1, c);
        }

        std::cout << "\n\nPlaintext deciphered: " << plain << std::endl;
    }
}
